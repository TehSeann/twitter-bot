from twython import Twython
from twython import TwythonError
import threading, time, json, os.path, sys

# Variables
appKey = "yourAppKey"
appSecret = "yourAppSecret"
accessToken = "yourAccessToken"
accessSecret = "yourAccessSecret"

twitter = Twython(app_key = appKey, app_secret = appSecret, oauth_token = accessToken, oauth_token_secret = accessSecret)
friends = twitter.get_friends_ids(screen_name="yourScreenName")

postlist = ignorelist = list()
rateLimit = searchLimit = [999, 999, 100]
justRetweeted = False

if os.path.isfile("ignorelist"):
    print("Loading Ignore List")
    with open("ignorelist") as f:
        ignorelist = f.read().splitlines()
    f.close()

def logText(text):
    # Log
    tmp = text.replace("\n", "")
    print(tmp)
    log = open("log", "a")
    log.write(tmp + "\n")
    log.close()

def checkError(r):
    # Error
    r = r.json()
    if "errors" in r:
        logText("Error: %s, Code: %s." %(r["errors"][0]["message"], r["errors"[0]["code"]]))

def checkRate():
    # Rate
    c = threading.Timer(10, checkRate)
    c.daemon = True;
    c.start()
    global rateLimit, searchLimit
    if rateLimit[2] < 10:
        print("Ratelimit too low, Cooldown (%s%)" %(str(ratelimit[2])))
        time.sleep(30)
    rate = twitter.get_application_rate_limit_status()
    for res_family in rate["resources"]:
        for res in rate["resources"][res_family]:
            limit = rate["resources"][res_family][res]["limit"]
            remaining = rate["resources"][res_family][res]["remaining"]
            percent = float(remaining) / float(limit) * 100
            if res == "/search/tweets":
                searchLimit = [limit, remaining, percent]
            if res == "/application/rate_limit_status":
                rateLimit = [limit, remaining, percent]
            if percent < 5.0:
                logText("%s -> %s: %s! < 5% Shutting Down." %(res_family, res, str(percent)))
                sys.exit("%s -> %s: %s! < 5% Shutting Down." %(res_family, res, str(percent)))
            elif percent < 30.0:
                logText("WARNING: %s -> %s: %s! < 30%" %(res_family, res, str(percent)))
            elif percent < 70.0:
                logText("%s -> %s: %s" %(res_family, res, percent))
def updateQueue():
    # Queue
    global justRetweeted
    if justRetweeted:
        u = threading.Timer(20, updateQueue)
        justRetweeted = False
    else:
        u = threading.Timer(1.8, updateQueue)
    u.daemon = True;
    u.start()
    erro = TwythonError
    if len(postlist) > 0:
        print("=== Checking Retweet Queue ===")
        print("Queue Length: %s" % str(len(postlist)))
        if not rateLimit[2] < 20:
            tweet = postlist[0]
            logText("Checking Tweet: %s" %(str(tweet["id"])))
            # logText("Checking Tweet: %s: %s" %(str(tweet["id"]), str(tweet["text"].encode("utf8"))))
            checkRT(tweet)
            checkLike(tweet)
            checkFollow(tweet)
            postlist.pop(0)
        else:
            print("Ratelimit at %s -> Pausing Competition Enters." %str(rateLimit[2]))

def checkRT(tweet):
    # Retweet
    global justRetweeted
    if any(x in tweet["text"].lower() for x in ["#rt", " rt", "retweet"]):
        try:
            twitter.retweet(id = tweet["id"])
            justRetweeted = True
            logText("Retweeted: %s" % tweet["id"])
        except TwythonError as e:
            logText(str(e))

def checkLike(tweet):
    # Like
    if any(x in tweet["text"].lower() for x in [" favourite", " fav ", "like ", "like,"]):
        try:
            twitter.create_favorite(id = tweet["id_str"])
            logText("Liked/Favourited: %s" % tweet["id"])
        except TwythonError as e:
            logText(str(e))
def checkFollow(tweet):
    # Follow
    if any(x in tweet["text"].lower() for x in [" follow", " follower"]):
        try:
            twitter.create_friendship(screen_name = tweet["user"]["screen_name"])
            logText("Followed: %s" % tweet['user']['screen_name'])
        except TwythonError as e:
            logText(str(e))

def getTweets():
    # Get Tweets
    t = threading.Timer(180, getTweets)
    t.daemon = True;
    t.start()
    global searchLimit
    if not searchLimit[2] < 40:
        print("=== Scanning for new Contests ===")
        for kw in ["#win", "#comp ", "#competition", "#giveaway"]:
            print("Getting Results for: %s" % kw)
            try:
                tweets = twitter.search(q="%s -filter:retweets AND -filter:replies" % kw, count=100, result_type='mixed')
                c = 0
                for tweet in tweets["statuses"]:
                    c = c + 1
                    id = oid = tweet["id"]
                    user = ouser = tweet["user"]
                    name = oname = user["screen_name"]
                    is_retweet = 0
                    if "retweeted_status" in tweet:
                        is_retweet = 1
                        otweet = tweet["retweeted_status"]
                        oid = otweet["id"]
                        ouser = otweet["user"]
                        oname = ouser["screen_name"]
                    if not oid in ignorelist:
                        if not oname in ignorelist:
                            if tweet["retweet_count"] > 0:
                                if is_retweet:
                                    postlist.append(otweet)
                                else:
                                    postlist.append(tweet)
                                ignore = open("ignorelist", "a")
                                if is_retweet:
                                    print("%s - %s retweeting %s - %s" %(id, name, oid, oname))
                                    # print("%s - %s retweeting %s - %s - %s" %(id, name, oid, oname, tweet["text"]))
                                    ignorelist.append(oid)
                                    ignore.write("%s\n" % oid)
                                else:
                                    print("%s - %s" %(id, name, ))
                                    ignorelist.append(id)
                                    ignore.write("%s\n" % id)
                                ignore.close()
                        # else:
                        #     if is_retweet:
                        #         print("%s Ingored: %s is in ignore list" %(id, oname))
                        #     else:
                        #         print("%s is in ignore list" % oname)
                    # else:
                    #     if is_retweet:
                    #         print("%s Ignored: %s is in ignore list" %(id, oid))
                    #     else:
                    #         print("%s is in ignore list" %(id))
                print("Retieved %s Competition Tweets!" % c)
            except TwythonError as e:
                logText("Error getting tweets: %s" % e)
    else:
        print("Skipping Search, Queue: %s, Ratelimit: %s/%s (%s%)" %(str(len(postlist)), str(searchLimit[1])), str(searchLimit[0]), str(searchLimit[2]))

checkRate()
getTweets()
updateQueue()

while(True):
    time.sleep(1)
